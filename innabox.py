import curses


globalvars = {'level': 0, 'location': [1,1], 'health': 100, 'inventory': []}

def main(mainwin):
	#Initialize Colors
	curses.init_pair(1,curses.COLOR_RED, curses.COLOR_BLUE)
	curses.init_pair(2,curses.COLOR_BLACK, curses.COLOR_WHITE)
	
	#Initialize the Main Window
	mainwin.bkgd(' ', curses.color_pair(1))
	bounds = mainwin.getmaxyx()
	
	#Initialize the Logo
	logobounds = ((bounds[1] // 2) - 20)
	writetext(mainwin, r"  ___                   _               ", logobounds, 2, curses.A_BLINK)
	writetext(mainwin, r" |_ _|_ __  _ __   __ _| |__   _____  __", logobounds, 3, curses.A_BLINK)
	writetext(mainwin, r"  | || '_ \| '_ \ / _` | '_ \ / _ \ \/ /", logobounds, 4, curses.A_BLINK)
	writetext(mainwin, r"  | || | | | | | | (_| | |_) | (_) >  < ", logobounds, 5, curses.A_BLINK)
	writetext(mainwin, r" |___|_| |_|_| |_|___,_|____/ \___/_/\_|", logobounds, 6, curses.A_BLINK)
	writetext(mainwin, r"                                        ", logobounds, 7, curses.A_BLINK)
	
	#Initialize the Main Menu
	menubounds = [((bounds[1] // 2) - 20), ((bounds[0] // 2) - 5)]
	menu = createwindow(menubounds[0], menubounds[1], 10, 40)
	menu.bkgd(' ', curses.color_pair(2))
	menu.box()
	
	#Draw Main Menu
	writetext(menu, "Start", 1, 1)
	writetext(menu, "Options", 1, 3)
	writetext(menu, "Exit", 1, 5)
	
	#Allow User to Choose
	selected = 0
	while 1:
		refresh(mainwin)
		refresh(menu)
		ichar = mainwin.getch()
		if ichar == ord('s'):
			selected += 1
		if ichar == ord('w'):
			selected -= 1
		if selected < 0:
			selected = 2
		if selected > 2:
			selected = 0
		#Highlight Options
		if selected == 0:
			writetext(menu, "Start", 1, 1, curses.A_STANDOUT)
			writetext(menu, "Options", 1, 3)
			writetext(menu, "Exit", 1, 5)
		if selected == 1:
			writetext(menu, "Start", 1, 1)
			writetext(menu, "Options", 1, 3, curses.A_STANDOUT)
			writetext(menu, "Exit", 1, 5)
		if selected == 2:
			writetext(menu, "Start", 1, 1)
			writetext(menu, "Options", 1, 3)
			writetext(menu, "Exit", 1, 5, curses.A_STANDOUT)
	
def createwindow(x, y, height, width):
	win = curses.newwin(height, width, y, x)
	return win

def writetext(window, string, x, y, form = curses.A_BOLD):
	window.addstr(y, x, string, form)

def move(window, x, y):
	window.move(y,x)
	refresh(window)	

def refresh(window):
	window.refresh()

if __name__ == "__main__":
	curses.wrapper(main)
